package hu.bme.igno1v.temalabor.virtualloragateway.gateway;

import hu.bme.igno1v.temalabor.virtualloragateway.FrameStringifyer;
import hu.bme.igno1v.temalabor.virtualloragateway.forwarder.CsvFileForwarder;
import hu.bme.igno1v.temalabor.virtualloragateway.forwarder.FrameForwarder;
import hu.bme.igno1v.temalabor.virtualloragateway.forwarder.MyDevicesMqttForwarder;
import hu.bme.igno1v.temalabor.virtualloragateway.forwarder.UbidotsHttpForwarder;
import hu.bme.igno1v.temalabor.virtualloragateway.payload.SimpleVariable;
import hu.bme.igno1v.temalabor.virtualloragateway.radio.Frame;
import hu.bme.igno1v.temalabor.virtualloragateway.radio.Radio;

import java.util.ArrayList;
import java.util.List;

/**
 * A virtual LoRa gateway that contains a Radio interface with possibly multiple channels,
 * and contains also forwarders that forward the received LoRa frames.
 */
public class Gateway implements Radio.RadioEventListener {

    /**
     * Radio interface of the gateway.
     */
    private Radio radio;

    /**
     * Forwarders used to forward the received LoRa packets to various destinations with various methods.
     */
    List<FrameForwarder> forwarders = new ArrayList<>();

    /**
     * Create the gateway and initialize the radio interfaces.
     *
     * @param radioChannels UDP port numbers for each radio channels.
     *                      All of them must be between 1 and 65535.
     * @throws IllegalArgumentException When a socket port number is invalid
     */
    public Gateway(int[] radioChannels) throws IllegalArgumentException {
        createRadio(radioChannels);
        radio.registerListener(this);
    }

    /**
     * Initialize the radio interface.
     *
     * @param radioChannels UDP port numbers for each radio channels.
     *                      All of them must be between 1 and 65535.
     * @throws IllegalArgumentException When a socket port number is invalid
     */
    private void createRadio(int[] radioChannels) throws IllegalArgumentException {
        radio = new Radio();
        for (int channel : radioChannels)
            radio.addChannel(channel);
    }

    /**
     * Add a frame forwarder to the gateway.
     *
     * @param forwarder Forwarder used to forward the frames received through the radio interface
     */
    public void addForwarder(FrameForwarder forwarder) {
        forwarders.add(forwarder);
    }

    /**
     * "Callback" method notifying the gateway that a new packet is received through the radio interface
     * and should be forwarded.
     *
     * @param frame The receved frame
     */
    @Override
    public void frameReceived(Frame frame) {

        String frmStr = FrameStringifyer.stringify(frame);
        System.out.println(frmStr);

        Frame.MType mtype = frame.getMytpeEnum();
        switch (mtype) {
            case UNCONFIRMED_DATA_UP:
            case CONFIRMED_DATA_UP:
                for (FrameForwarder forwarder : forwarders)
                    forwarder.forwardFrame(frame);
                if (mtype == Frame.MType.CONFIRMED_DATA_UP) {
                    // Send ACK
                }
                break;
            default:
                System.out.println("Can't handle MType \"" + mtype + "\"");
                break;
        }

    }

}

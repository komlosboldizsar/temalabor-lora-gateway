package hu.bme.igno1v.temalabor.virtualloragateway.loader;

import com.sun.istack.internal.NotNull;
import hu.bme.igno1v.temalabor.virtualloragateway.forwarder.CsvFileForwarder;
import hu.bme.igno1v.temalabor.virtualloragateway.loader.exception.LoaderException;
import hu.bme.igno1v.temalabor.virtualloragateway.loader.exception.LoaderParseException;
import hu.bme.igno1v.temalabor.virtualloragateway.payload.Variable;
import org.jdom2.Attribute;
import org.jdom2.DataConversionException;
import org.jdom2.Element;

import java.util.Map;

/**
 * Class for loading/instantiating a CsvFileForwarder from an XML configuration file.
 *
 * @see CsvFileForwarder
 * <p>
 * XML example (<ubidots_http> tag must be a child of the <forwarders> tag in the configuration file):
 * <pre>
 *     <!-- [boolean] = true, on, 1, yes, false, off, 0, no -->
 *     <csv_file
 *         <!-- Attribute is needed, must be a valid file path -->
 *         file="lora.csv"
 *         <!-- Attribute is optional, valid values: [boolean] -->
 *         printdeviceid="true"
 *         <!-- Attribute is optional, valid values: [boolean] -->
 *         printtimestamp="false"
 *         <!-- Attribute is optional, must contain only one character -->
 *         delimiter=","
 *         <!-- Attribute is optional: used for console logging -->
 *         customlogtag="logtag">
 *
 *         <!-- 0..N variables -->
 *         <variable
 *             <!-- Attribute is needed, must be a non-negative integer -->
 *             decimals="3"
 *             <!-- Attribute is used to bind to bindable variables created under the <variables> tag -->
 *             <!-- Attribute most contain the name (reference) of an existing variable -->
 *             reference="accelerometer_x"/>
 *
 *     </ubidots_http>
 * </pre>
 */
class CsvFileForwarderLoader {

    /**
     * Instantiate a CsvFileForwarder object from the given XML element (see expected format above at class description)
     * and using the given bindable variables.
     *
     * @param xmlElement CsvFileForwarder descriptor XML element
     * @param variables  Usable bindable variables in reference -> variable form
     * @return The instantiated CsvFileForwarder object
     * @throws LoaderException LoaderException Invalid configuration format: missing element/attribute, invalid attribute value
     */
    static CsvFileForwarder createInstance(@NotNull final Element xmlElement, @NotNull final Map<String, Variable> variables) throws LoaderException {

        Attribute fileAttribute = xmlElement.getAttribute("file");
        if (fileAttribute == null)
            throw new LoaderParseException("'file' attribute is missing for a <csv_file> tag.");
        String file = fileAttribute.getValue();

        CsvFileForwarder.Builder forwarderBuilder = new CsvFileForwarder.Builder(file);

        Attribute printdeviceidAttribute = xmlElement.getAttribute("printdeviceid");
        if (printdeviceidAttribute != null) {
            try {
                forwarderBuilder.setPrintDeviceId(printdeviceidAttribute.getBooleanValue());
            } catch (DataConversionException e) {
                throw new LoaderParseException("Value of 'printdeviceid' attribute is not a valid boolean value for a <csv_file> tag.", e);
            }
        }

        Attribute printtimestampAttribute = xmlElement.getAttribute("printtimestamp");
        if (printtimestampAttribute != null) {
            try {
                forwarderBuilder.setPrintTimestamp(printtimestampAttribute.getBooleanValue());
            } catch (DataConversionException e) {
                throw new LoaderParseException("Value of 'printtimestamp' attribute is not a valid boolean value for a <csv_file> tag.", e);
            }
        }

        Attribute delimiterAttribute = xmlElement.getAttribute("delimiter");
        if (delimiterAttribute != null) {
            String delimiterAttributeValue = delimiterAttribute.getValue();
            if (delimiterAttributeValue.length() != 1)
                throw new LoaderParseException("'delimiter' attribute for a <csv_file> tag must a character (string with length of 1).");
            forwarderBuilder.setDelimiter(delimiterAttributeValue.charAt(0));
        }

        for (Element variableElement : xmlElement.getChildren("variable")) {

            int decimals;
            Attribute decimalsAttribute = variableElement.getAttribute("decimals");
            if (decimalsAttribute == null) {
                throw new LoaderParseException("'decimals' attribute is missing for a <variable> (child of <csv_file>) tag.");
            }
            try {
                decimals = Integer.parseInt(decimalsAttribute.getValue());
            } catch (NumberFormatException e) {
                throw new LoaderParseException("'decimals' attribute for a <variable> (child of <csv_file>) tag must be an integer.", e);
            }

            Attribute referenceAttribute = variableElement.getAttribute("reference");
            if (referenceAttribute == null)
                throw new LoaderParseException("'reference' attribute is missing for a <variable> (child of <csv_file>) tag.");
            String reference = referenceAttribute.getValue();

            Variable variable = variables.get(reference);
            if (variable == null) {
                throw new LoaderParseException("Variable with reference '" + reference + "' doesn't exists.");
            }
            try {
                forwarderBuilder.addVariable(variable, decimals);
            } catch (IllegalArgumentException e) {
                throw new LoaderParseException("Can't create variable for CsvFileForwarder: " + e.getMessage(), e);
            }

        }

        CsvFileForwarder forwarder = forwarderBuilder.build();

        Attribute customlogtagAttribute = xmlElement.getAttribute("customlogtag");
        if (customlogtagAttribute != null)
            forwarder.setCustomLogTag(customlogtagAttribute.getValue());

        return forwarder;

    }

}

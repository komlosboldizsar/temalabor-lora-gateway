package hu.bme.igno1v.temalabor.virtualloragateway.loader;

import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;
import hu.bme.igno1v.temalabor.virtualloragateway.forwarder.FrameForwarder;
import hu.bme.igno1v.temalabor.virtualloragateway.gateway.Gateway;
import hu.bme.igno1v.temalabor.virtualloragateway.loader.exception.LoaderException;
import hu.bme.igno1v.temalabor.virtualloragateway.loader.exception.LoaderParseException;
import hu.bme.igno1v.temalabor.virtualloragateway.payload.SimpleVariable;
import hu.bme.igno1v.temalabor.virtualloragateway.payload.Variable;
import org.jdom2.*;
import org.jdom2.input.SAXBuilder;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class for loading/instantiating a Gateway from an XML configuration file.
 *
 * @see Gateway
 * <p>
 * XML example:
 * <pre>
 *     <virtual_lora_gateway_config>
 *         <radio>
 *             <channel port="20400" />
 *         </radio>
 *         <variables>
 *             <simplevariable reference="accelerometer_x" offset="0" length="2" whole="1" signed="true" />
 *             <!-- Attribute 'reference' is used to bind forwarder variables -->
 *             <!-- More variables -->
 *         </variables>
 *         <forwarders>
 *             <!-- See another Loader classes for details -->
 *             <ubidots_http ...>...</ubidots_http>
 *             <mydevices_mqtt ...>...</mydevices_mqtt>
 *             <csv_file ...>...</csv_file>
 *         </forwarders>
 *     </virtual_lora_gateway_config>
 * </pre>
 */
public class GatewayLoader {

    /**
     * Path of the XML file to load and parse.
     */
    private final String xmlPath;

    /**
     * The gateway that has been built from the parsed configuration.
     */
    private Gateway builtGateway;

    /**
     * Collection of bindable variables.
     */
    private Map<String, Variable> variables = new HashMap<>();

    /**
     * Create the GatewayLoader object. XML is not parsed and gateway is not instantiated in the constructor.
     *
     * @param xmlPath Path of the XML file to load and parse.
     * @see GatewayLoader#load
     */
    public GatewayLoader(@NotNull final String xmlPath) {
        this.xmlPath = xmlPath;
    }

    /**
     * Parse the XML file and instantiate the Gateway.
     *
     * @return The created Gateway instance.
     * @throws LoaderException Thrown if file is not readable or exception during parsing
     *                         (invalid XML; invalid configuration format:  missing element/attribute, invalid attribute value)
     */
    @Nullable
    public Gateway load() throws LoaderException {
        try {
            File xmlFile = new File(xmlPath);
            SAXBuilder saxBuilder = new SAXBuilder();
            Document document = saxBuilder.build(xmlFile);
            Element rootElement = document.getRootElement();
            interpretDocument(rootElement);
            return builtGateway;
        } catch (JDOMException e) {
            throw new LoaderParseException("JDOMException while parsing XML configuration file:" + e.getMessage(), e);
        } catch (IOException e) {
            throw new LoaderException("IOException while reading XML configuration file: " + e.getMessage(), e);
        }
    }

    /**
     * Parse and interpret the root XML element of the configuration file.
     *
     * @param document Root XML element
     * @throws LoaderException Invalid configuration format: missing element/attribute, invalid attribute value
     */
    @Nullable
    public void interpretDocument(@NotNull final Element document) throws LoaderException {

        Element radioElement = document.getChild("radio");
        if (radioElement == null)
            throw new LoaderParseException("<radio> tag is missing.");
        interpretRadioElement(radioElement);

        Element variablesElement = document.getChild("variables");
        if (variablesElement == null)
            throw new LoaderParseException("<variables> tag is missing.");
        interpretVariablesElement(variablesElement);

        Element forwardersElement = document.getChild("forwarders");
        if (forwardersElement == null)
            throw new LoaderParseException("<forwarders> tag is missing.");
        interpretForwardersElement(forwardersElement);

    }

    /**
     * Parse and interpret the <radio> child of the root element.
     * Create the gateway, it's radio and it's channels.
     *
     * @param radioElement The first <radio> child of the root XML element.
     * @throws LoaderException Invalid configuration format: missing element/attribute, invalid attribute value
     */
    private void interpretRadioElement(@NotNull final Element radioElement) throws LoaderException {

        List<Element> channelElements = radioElement.getChildren("channel");
        int[] radioChannelsArray = new int[channelElements.size()];
        int c = 0;

        for (Element channelElement : channelElements) {

            Attribute portAttribute = channelElement.getAttribute("port");
            if (portAttribute == null)
                throw new LoaderParseException("'port' attribute is missing for a <channel> tag.");

            try {
                radioChannelsArray[c++] = Integer.parseInt(portAttribute.getValue());
            } catch (NumberFormatException e) {
                throw new LoaderParseException("'port' attribute for a <channel> tag must be an integer.", e);
            }

        }

        try {
            builtGateway = new Gateway(radioChannelsArray);
        } catch (IllegalArgumentException e) {
            throw new LoaderParseException("Can't create Gateway: " + e.getMessage());
        }

    }

    /**
     * Parse and interpret the <variables> child of the root element.
     * Create the bindable variables.
     *
     * @param variablesElement The first <variables> child of the root XML element.
     * @throws LoaderException Invalid configuration format: missing element/attribute, invalid attribute value
     */
    private void interpretVariablesElement(@NotNull final Element variablesElement) throws LoaderException {

        List<Element> variablesChildrenElements = variablesElement.getChildren();
        for (Element variablesChildElement : variablesChildrenElements) {
            switch (variablesChildElement.getName()) {
                case "simplevariable":
                    interpretSimpevariableElement(variablesChildElement);
                    break;
            }
        }

    }

    /**
     * Default value of the 'length' attribute of a <simplevariable> tag when not given.
     */
    private final static int SIMPLEVARIABLE_LENGTH_DEFAULT = 1;

    /**
     * Default value of the 'signed' attribute of a <simplevariable> tag when not given.
     */
    private final static boolean SIMPLEVARIABLE_SIGNED_DEFAULT = false;

    /**
     * Parse and interpret a <simplevariable> child of the <variables> element.
     * Create a SimpleVariable object.
     *
     * @param simplevariableElement A <simplevariableElement> child of the <variables> XML element.
     * @throws LoaderException Invalid configuration format: missing element/attribute, invalid attribute value
     */
    private void interpretSimpevariableElement(@NotNull final Element simplevariableElement) throws LoaderException {

        String reference;
        Attribute referenceAttribute = simplevariableElement.getAttribute("reference");
        if (referenceAttribute == null)
            throw new LoaderParseException("'port' attribute is missing for a <simplevariable> tag.");
        reference = referenceAttribute.getValue();

        int offset;
        Attribute offsetAttribute = simplevariableElement.getAttribute("offset");
        if (offsetAttribute == null) {
            throw new LoaderParseException("'offset' attribute is missing for a <channel> tag.");
        }
        try {
            offset = Integer.parseInt(offsetAttribute.getValue());
        } catch (NumberFormatException e) {
            throw new LoaderParseException("'offset' attribute for a <simplevariable> tag must be an integer.", e);
        }

        int length = SIMPLEVARIABLE_LENGTH_DEFAULT;
        Attribute lengthAttribute = simplevariableElement.getAttribute("length");
        if (lengthAttribute != null) {
            try {
                length = Integer.parseInt(lengthAttribute.getValue());
            } catch (NumberFormatException e) {
                throw new LoaderParseException("'length' attribute for a <simplevariable> tag must be an integer.", e);
            }
        }

        int whole;
        Attribute wholeAttribute = simplevariableElement.getAttribute("whole");
        if (wholeAttribute == null) {
            throw new LoaderParseException("'whole' attribute is missing for a <channel> tag.");
        }
        try {
            whole = Integer.parseInt(wholeAttribute.getValue());
        } catch (NumberFormatException e) {
            throw new LoaderParseException("'whole' attribute for a <simplevariable> tag must be an integer.", e);
        }

        boolean signed = SIMPLEVARIABLE_SIGNED_DEFAULT;
        Attribute signedAttribute = simplevariableElement.getAttribute("signed");
        if (signedAttribute != null) {
            try {
                signed = signedAttribute.getBooleanValue();
            } catch (DataConversionException e) {
                throw new LoaderParseException("Value of 'signed' attribute is not a valid boolean value for a <simplevariable> tag.", e);
            }
        }

        try {
            SimpleVariable variable = new SimpleVariable(offset, length, whole, signed);
            variables.put(reference, variable);
        } catch (IllegalArgumentException e) {
            throw new LoaderParseException("Can't create SimpleVariable: " + e.getMessage(), e);
        }

    }

    /**
     * Parse and interpret the <forwardersElement> child of the root element.
     * Create the forwarders with the help of other Loader classes.
     *
     * @param forwardersElement The first <variables> child of the root XML element.
     * @throws LoaderException Invalid configuration format: missing element/attribute, invalid attribute value
     */
    private void interpretForwardersElement(@NotNull final Element forwardersElement) throws LoaderException {

        List<Element> forwardersChildrenElements = forwardersElement.getChildren();
        for (Element forwardersChildElement : forwardersChildrenElements) {
            FrameForwarder forwarder;
            switch (forwardersChildElement.getName()) {
                case "ubidots_http":
                    forwarder = UbidotsHttpForwarderLoader.createInstance(forwardersChildElement, variables);
                    break;
                case "mydevices_mqtt":
                    forwarder = MyDevicesMqttForwarderLoader.createInstance(forwardersChildElement, variables);
                    break;
                case "csv_file":
                    forwarder = CsvFileForwarderLoader.createInstance(forwardersChildElement, variables);
                    break;
                default:
                    throw new LoaderParseException("Unknown forwarder found in the <forwarders> tag.");
            }
            if (forwarder != null)
                builtGateway.addForwarder(forwarder);
        }

    }

}

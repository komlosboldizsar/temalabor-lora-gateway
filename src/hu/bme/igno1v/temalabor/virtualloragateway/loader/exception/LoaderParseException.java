package hu.bme.igno1v.temalabor.virtualloragateway.loader.exception;

public class LoaderParseException extends LoaderException {

    /**
     * Text displayed before the exception message.
     */
    private final static String BEFORE_MESSAGE = "Exception while parsing XML configuration file.";

    /**
     * Default constructor without message or cause.
     * Message will be initialized to the value of {BEFORE_MESSAGE}.
     */
    public LoaderParseException() {
        super(BEFORE_MESSAGE);
    }

    /**
     * Create a LoaderParseException and initialize the error message.
     * Message will be appended after the value of {BEFORE_MESSAGE}.
     * The cause will be initialized to null.
     *
     * @param message Message for the exception (short description of the cause, etc.)
     */
    public LoaderParseException(final String message) {
        super(BEFORE_MESSAGE + " " + message);
    }

    /**
     * Create a LoaderException and initialize the message and cause fields.
     * Message will be appended after the value of {BEFORE_MESSAGE}.
     *
     * @param message Message for the exception (short description of the cause, etc.)
     * @param cause   Cause of the exception
     */
    public LoaderParseException(final String message, final Throwable cause) {
        super(BEFORE_MESSAGE + " " + message, cause);
    }

}

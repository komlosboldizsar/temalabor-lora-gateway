package hu.bme.igno1v.temalabor.virtualloragateway.loader.exception;

/**
 * Exception for signaling that an error occurred while loading a gateway configuration file.
 */
public class LoaderException extends Exception {

    /**
     * Default constructor without message or cause. Both will be initialized to null.
     */
    public LoaderException() {
        super();
    }

    /**
     * Create a LoaderException and initialize the error message.
     * The cause will be initialized to null.
     *
     * @param message Message for the exception (short description of the cause, etc.)
     */
    public LoaderException(final String message) {
        super(message);
    }

    /**
     * Create a LoaderException and initialize the message and cause fields.
     *
     * @param message Message for the exception (short description of the cause, etc.)
     * @param cause   Cause of the exception
     */
    public LoaderException(final String message, final Throwable cause) {
        super(message, cause);
    }

}

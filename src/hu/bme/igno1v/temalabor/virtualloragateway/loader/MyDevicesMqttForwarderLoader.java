package hu.bme.igno1v.temalabor.virtualloragateway.loader;

import com.sun.istack.internal.NotNull;
import hu.bme.igno1v.temalabor.virtualloragateway.forwarder.MyDevicesMqttForwarder;
import hu.bme.igno1v.temalabor.virtualloragateway.loader.exception.LoaderException;
import hu.bme.igno1v.temalabor.virtualloragateway.loader.exception.LoaderParseException;
import hu.bme.igno1v.temalabor.virtualloragateway.payload.Variable;
import org.jdom2.Attribute;
import org.jdom2.Element;

import java.util.Map;

/**
 * Class for loading/instantiating a MyDevicesMqttForwarder from an XML configuration file.
 *
 * @see MyDevicesMqttForwarder
 * <p>
 * XML example (<mydevices_mqtt> tag must be a child of the <forwarders> tag in the configuration file):
 * <pre>
 *     <mydevices_mqtt
 *         <!-- Attribute is needed: client ID got at device creation -->
 *         clientid="fv43b65-54n1-11e7-fv5z-v32fv4312nhg"
 *         <!-- Attribute is needed: username got at device creation -->
 *         username="fsf3243-fdsa-r324-b0e9-rvr4v43r4828"
 *         <!-- Attribute is needed: password got at device creation -->
 *         password="g4komp43ni435jn654klfr5628"
 *         <!-- Attribute is optional: used for console logging -->
 *         customlogtag="logtag">
 *
 *         <!-- 0..N variables -->
 *         <variable
 *             <!-- Attribute is needed, must be a non-negative integer -->
 *             <!-- Used in the MQTT topic string -->
 *             channel="0"
 *             <!-- Attribute is needed, see MyDevices MQTT API documentation for possible values -->
 *             type="gx"
 *             <!-- Attribute is needed, see MyDevices MQTT API documentation for possible values -->
 *             unit="ms2"
 *             <!-- Attribute is used to bind to bindable variables created under the <variables> tag -->
 *             <!-- Attribute most contain the name (reference) of an existing variable -->
 *             reference="accelerometer_x" />
 *
 *     </mydevices_mqtt>
 * </pre>
 */
class MyDevicesMqttForwarderLoader {

    /**
     * Instantiate a MyDevicesMqttForwarder object from the given XML element (see expected format above at class description)
     * and using the given bindable variables.
     *
     * @param xmlElement MyDevicesMqttForwarder descriptor XML element
     * @param variables  Usable bindable variables in reference -> variable form
     * @return The instantiated MyDevicesMqttForwarder object
     * @throws LoaderException LoaderException Invalid configuration format: missing element/attribute, invalid attribute value
     */
    static MyDevicesMqttForwarder createInstance(@NotNull final Element xmlElement, @NotNull final Map<String, Variable> variables) throws LoaderException {

        Attribute clientidAttribute = xmlElement.getAttribute("clientid");
        if (clientidAttribute == null)
            throw new LoaderParseException("'clientid' attribute is missing for a <mydevices_mqtt> tag.");
        String clientid = clientidAttribute.getValue();

        Attribute usernameAttribute = xmlElement.getAttribute("username");
        if (usernameAttribute == null)
            throw new LoaderParseException("'username' attribute is missing for a <mydevices_mqtt> tag.");
        String username = usernameAttribute.getValue();

        Attribute passwordAttribute = xmlElement.getAttribute("password");
        if (passwordAttribute == null)
            throw new LoaderParseException("'password' attribute is missing for a <mydevices_mqtt> tag.");
        String password = passwordAttribute.getValue();

        MyDevicesMqttForwarder forwarder = new MyDevicesMqttForwarder(clientid, username, password);

        Attribute customlogtagAttribute = xmlElement.getAttribute("customlogtag");
        if (customlogtagAttribute != null)
            forwarder.setCustomLogTag(customlogtagAttribute.getValue());

        for (Element variableElement : xmlElement.getChildren("variable")) {

            int channel;
            Attribute channelAttribute = variableElement.getAttribute("channel");
            if (channelAttribute == null) {
                throw new LoaderParseException("'channel' attribute is missing for a <variable> (child of <mydevices_mqtt>) tag.");
            }
            try {
                channel = Integer.parseInt(channelAttribute.getValue());
            } catch (NumberFormatException e) {
                throw new LoaderParseException("'channel' attribute for a <variable> (child of <mydevices_mqtt>) tag must be an integer.", e);
            }

            Attribute typeAttribute = variableElement.getAttribute("type");
            if (typeAttribute == null)
                throw new LoaderParseException("'type' attribute is missing for a <variable> (child of <mydevices_mqtt>) tag.");
            String type = typeAttribute.getValue();

            Attribute unitAttribute = variableElement.getAttribute("unit");
            if (unitAttribute == null)
                throw new LoaderParseException("'unit' attribute is missing for a <variable> (child of <mydevices_mqtt>) tag.");
            String unit = unitAttribute.getValue();

            Attribute referenceAttribute = variableElement.getAttribute("reference");
            if (referenceAttribute == null)
                throw new LoaderParseException("'reference' attribute is missing for a <variable> (child of <mydevices_mqtt>) tag.");
            String reference = referenceAttribute.getValue();

            Variable variable = variables.get(reference);
            if (variable == null)
                throw new LoaderParseException("Variable with reference '" + reference + "' doesn't exists.");

            try {
                forwarder.addVariable(variable, channel, type, unit);
            } catch (IllegalArgumentException e) {
                throw new LoaderParseException("Can't create variable for MyDevicesMqttForwarder: " + e.getMessage(), e);
            }

        }

        return forwarder;

    }

}

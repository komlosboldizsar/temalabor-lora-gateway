package hu.bme.igno1v.temalabor.virtualloragateway.loader;

import com.sun.istack.internal.NotNull;
import hu.bme.igno1v.temalabor.virtualloragateway.forwarder.UbidotsHttpForwarder;
import hu.bme.igno1v.temalabor.virtualloragateway.loader.exception.LoaderException;
import hu.bme.igno1v.temalabor.virtualloragateway.loader.exception.LoaderParseException;
import hu.bme.igno1v.temalabor.virtualloragateway.payload.Variable;
import org.jdom2.Attribute;
import org.jdom2.Element;

import java.util.Map;

/**
 * Class for loading/instantiating a UbidotsHttpForwarder from an XML configuration file.
 *
 * @see UbidotsHttpForwarder
 * <p>
 * XML example (<ubidots_http> tag must be a child of the <forwarders> tag in the configuration file):
 * <pre>
 *     <ubidots_http
 *         <!-- Attribute is needed: Ubidots token from Dashboard-->
 *         token="A1E-4324oojmf43oijmr439uk342b"
 *         <!-- Attribute is optional: used for console logging -->
 *         customlogtag="logtag">
 *
 *         <!-- 0..N variables -->
 *         <variable
 *             <!-- Attribute is needed: variable ID from Ubidots Dashboard -->
 *             id="mi4o398jmd32kmu2"
 *             <!-- Attribute is used to bind to bindable variables created under the <variables> tag -->
 *             <!-- Attribute most contain the name (reference) of an existing variable -->
 *             reference="accelerometer_x" />
 *     </ubidots_http>
 * </pre>
 */
class UbidotsHttpForwarderLoader {

    /**
     * Instantiate a UbidotsHttpForwarder object from the given XML element (see expected format above at class description)
     * and using the given bindable variables.
     *
     * @param xmlElement UbidotsHttpForwarder descriptor XML element
     * @param variables  Usable bindable variables in reference -> variable form
     * @return The instantiated UbidotsHttpForwarder object
     * @throws LoaderException LoaderException Invalid configuration format: missing element/attribute, invalid attribute value
     */
    static UbidotsHttpForwarder createInstance(@NotNull final Element xmlElement, @NotNull final Map<String, Variable> variables) throws LoaderException {

        Attribute tokenAttribute = xmlElement.getAttribute("token");
        if (tokenAttribute == null)
            throw new LoaderParseException("'token' attribute is missing for a <ubidots_http> tag.");
        String token = tokenAttribute.getValue();

        UbidotsHttpForwarder forwarder = new UbidotsHttpForwarder(token);

        Attribute customlogtagAttribute = xmlElement.getAttribute("customlogtag");
        if (customlogtagAttribute != null)
            forwarder.setCustomLogTag(customlogtagAttribute.getValue());

        for (Element variableElement : xmlElement.getChildren("variable")) {

            Attribute idAttribute = variableElement.getAttribute("id");
            if (idAttribute == null)
                throw new LoaderParseException("'id' attribute is missing for a <variable> (child of <ubidots_http>) tag.");
            String id = idAttribute.getValue();

            Attribute referenceAttribute = variableElement.getAttribute("reference");
            if (referenceAttribute == null)
                throw new LoaderParseException("'reference' attribute is missing for a <variable> (child of <ubidots_http>) tag.");
            String reference = referenceAttribute.getValue();

            Variable variable = variables.get(reference);
            if (variable == null)
                throw new LoaderParseException("Variable with reference '" + reference + "' doesn't exists.");

            forwarder.addVariable(variable, id);

        }

        return forwarder;

    }

}

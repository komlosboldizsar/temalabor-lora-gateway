package hu.bme.igno1v.temalabor.virtualloragateway.forwarder;

import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;
import hu.bme.igno1v.temalabor.virtualloragateway.payload.Variable;
import hu.bme.igno1v.temalabor.virtualloragateway.radio.Frame;

import java.util.LinkedList;
import java.util.List;

/**
 * Class for forwarding incoming LoRa packets to the myDevices platform through MQTT protocol.
 */
public class MyDevicesMqttForwarder extends MqttForwarder {

    /**
     * MQTT username got at device creation on the dashboard.
     */
    private final String username;

    /**
     * MQTT client ID got at device creation on the dashboard.
     */
    private final String clientId;

    /**
     * Variables sent to the myDevices platform.
     * Each variable is published individually.
     */
    private final List<MyDevicesVariable> variables = new LinkedList<>();

    /**
     * Address/URL of the myDevices MQTT broker.
     * Protocol, host and port must be given in the known protocol://host:port format.
     */
    private static final String MYDEVICES_BROKER = "tcp://mqtt.mydevices.com:1883";

    /**
     * Initialize the MQTT forwarder and fill needed constant fields.
     * Sets the custom log tag to the value of {MYDEVICES_MQTT_LOG_TAG}.
     *
     * @param clientId MQTT client ID got at device creation on the dashboard
     * @param username MQTT uesrname got at device creation on the dashboard
     * @param password MQTT password got at device creation on the dashboard
     */
    public MyDevicesMqttForwarder(@NotNull final String clientId, @NotNull final String username, @NotNull final String password) {
        super(MYDEVICES_BROKER, clientId, username, password);
        this.username = username;
        this.clientId = clientId;
        setCustomLogTag(null);
    }

    /**
     * Add a variable that's value should be published to the myDevices platform when a new LoRa frame arrives.
     *
     * @param variable The variable object where value is read from
     * @param channel  Channel number used in the publication topic (should be unique for each variable)
     *                 Negative values are not valid and IllegalArgumentException is thrown.
     * @param type     Type of the sensor that's the source of the variable data
     *                 See API documentation for possible values
     * @param unit     Unit of the value of the variable
     *                 See API documentation for possible values
     * @throws IllegalArgumentException If value of the channel parameters is invalid (<0).
     */
    public void addVariable(@NotNull final Variable variable, int channel, @NotNull final String type, @NotNull final String unit) throws IllegalArgumentException {
        variables.add(new MyDevicesVariable(variable, channel, type, unit));
    }

    /**
     * Forward the frame to the myDevices IoT platform through MQTT protocol.
     * Each stored variable is send individually to the v1/{username}/things/{clientId}/data/{MyDevicesVariable.channel} topic
     *
     * @param frame Frame to forward
     */
    @Override
    public void forwardFrame(Frame frame) {
        byte[] frmpayload = frame.getFrmpayload();
        for (MyDevicesVariable mdvar : variables) {
            String topic = String.format("v1/%s/things/%s/data/%d", username, clientId, mdvar.channel);
            float value = mdvar.variable.getValue(frmpayload);
            String message = String.format("%s,%s=%f", mdvar.type, mdvar.unit, value);
            publishAsynchronously(topic, message);
        }
    }

    /**
     * Class for storing data for variables that are published to the myDevices platform by this forwarder.
     */
    private class MyDevicesVariable {

        /**
         * Variable that's value is published.
         */
        public final Variable variable;

        /**
         * Channel number used in the publication topic (should be unique for each variable).
         */
        public final int channel;

        /**
         * Type of the sensor that's the source of the variable data.
         * See API documentation for possible values.
         */
        public final String type;

        /**
         * Unit of the value of the variable.
         * See API documentation for possible values.
         */
        public final String unit;

        /**
         * Initialize the data structure and fill the fields.
         *
         * @param variable Variable that's value is published
         * @param channel  Channel number used in the publication topic (should be unique for each variable)
         *                 Negative values are not valid and IllegalArgumentException is thrown.
         * @param type     Type of the sensor that's the source of the variable data
         *                 See API documentation for possible values
         * @param unit     Unit of the value of the variable
         *                 See API documentation for possible values
         * @throws IllegalArgumentException If value of the channel parameters is invalid (<0).
         */
        public MyDevicesVariable(@NotNull final Variable variable, int channel, @NotNull final String type, @NotNull final String unit) throws IllegalArgumentException {

            if (channel < 0)
                throw new IllegalArgumentException("Value of the channel attribute must be non-negative!");

            this.variable = variable;
            this.channel = channel;
            this.type = type;
            this.unit = unit;

        }

    }

    /**
     * Tag used for every log message before the custom log tag.
     */
    private final static String MYDEVICES_MQTT_LOG_TAG = "MYDEVICES";

    /**
     * Set the custom log tag.
     *
     * @param customLogTag Tag used with the {MYDEVICES_MQTT_LOG_TAG} tag when logging.
     *                     If not null, {MYDEVICES_MQTT_LOG_TAG} and custom log tag will be separated by ::.
     *                     If null, only the {MYDEVICES_MQTT_LOG_TAG} is shown.
     */
    public void setCustomLogTag(@Nullable String customLogTag) {
        if (customLogTag == null)
            super.setCustomLogTag(MYDEVICES_MQTT_LOG_TAG);
        else
            super.setCustomLogTag(MYDEVICES_MQTT_LOG_TAG + "::" + customLogTag);
    }

}

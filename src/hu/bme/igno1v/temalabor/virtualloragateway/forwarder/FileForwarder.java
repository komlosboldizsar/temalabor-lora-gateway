package hu.bme.igno1v.temalabor.virtualloragateway.forwarder;

import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;
import hu.bme.igno1v.temalabor.virtualloragateway.StringUtils;
import hu.bme.igno1v.temalabor.virtualloragateway.radio.Frame;

import java.io.*;

/**
 * Abstract base class for saving LoRa packets to file.
 */
public abstract class FileForwarder implements FrameForwarder {

    /**
     * Write the frame to a file
     *
     * @param frame Frame to write
     */
    @Override
    public void forwardFrame(Frame frame) {
        String contents = getContents(frame);
        writeToFile(contents);
    }

    /**
     * Get the string to write to the file for a LoRa frame.
     *
     * @param frame Frame that's contents and meta-informations will be used to build the string that is written to the file
     * @return Contents to write to the file
     */
    protected abstract String getContents(Frame frame);

    /**
     * Get the path of the file used for the next write operation.
     *
     * @return Path of the file to write
     */
    protected abstract String getFilePath();

    /**
     * Get if contents should be appended to the file during the next write operation if it already exists
     *
     * @return true if 'append' should be used as file open/write method; false if file can be overwritten
     */
    protected abstract boolean useAppend();

    /**
     * Write the string given in the {contents} parameter to the file specified by {getFilePath()} method.
     * This method uses 'append' as file open/write method if {useAppend()} returns true.
     *
     * @param contents String to write to the file
     */
    private void writeToFile(String contents) {
        String filePath = getFilePath();
        boolean append = useAppend();
        try (FileWriter fileWriter = new FileWriter(filePath, append)) {
            fileWriter.append(contents);
            log(String.format("%s \"%s\" to file \"%s\"", (append ? "Appended" : "Written"), contents, filePath));
        } catch (IOException e) {
            log(String.format("Writing to file \"%s\" failed, reason: %s", filePath, e.getMessage()));
        }
    }

    /**
     * Tag used after the {FILE_LOG_TAG} tag when logging.
     */
    protected String customLogTag = "";

    /**
     * Tag used for every log message before the custom log tag.
     */
    private final static String FILE_LOG_TAG = "[FORWARDER:FILE]";

    /**
     * Write a tagged log message to the console.
     * Newline characters in the message are converted by {StringUtils.replaceNewlines}.
     *
     * @param message Message to log
     */
    protected void log(@NotNull String message) {
        message = StringUtils.replaceNewlines(message);
        System.out.println(FILE_LOG_TAG + customLogTag + " " + message);
    }

    /**
     * Set the custom log tag.
     *
     * @param customLogTag Tag used after the {FILE_LOG_TAG} tag when logging.
     *                     If not null, will be converted to [customLogTag] format.
     *                     If null, custom log tag will not be displayed.
     */
    public void setCustomLogTag(@Nullable String customLogTag) {
        if (customLogTag == null)
            this.customLogTag = "";
        else
            this.customLogTag = "[" + customLogTag + "]";
    }

}

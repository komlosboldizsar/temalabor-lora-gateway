package hu.bme.igno1v.temalabor.virtualloragateway.forwarder;

import hu.bme.igno1v.temalabor.virtualloragateway.radio.Frame;

/**
 * Interface of classes that forward a LoRa frame to various destinations with various methods.
 */
public interface FrameForwarder {

    /**
     * Forward a frame to the destination defined in the implementing class with the method
     * defined in the implementing method.
     *
     * @param frame Frame to forward
     */
    void forwardFrame(Frame frame);

}

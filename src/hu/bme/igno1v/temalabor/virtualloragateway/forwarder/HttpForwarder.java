package hu.bme.igno1v.temalabor.virtualloragateway.forwarder;

import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;
import hu.bme.igno1v.temalabor.virtualloragateway.StringUtils;
import hu.bme.igno1v.temalabor.virtualloragateway.radio.Frame;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Abstract base class for forwarding LoRa packets through HTTP (most cases with REST API).
 */
public abstract class HttpForwarder implements FrameForwarder {

    /**
     * Forward the frame through HTTP protocol.
     *
     * @param frame Frame to forward
     */
    @Override
    public void forwardFrame(Frame frame) {
        String requestBody = createRequestBody(frame);
        executePostAsynchronously(requestBody);
    }

    /**
     * Create the body of the HTTP request.
     *
     * @param frame Frame to forward
     * @return Body (content) of the HTTP request
     */
    protected abstract String createRequestBody(Frame frame);

    /**
     * Get the URL of the HTTP request.
     *
     * @return URL of the HTTP request
     */
    protected abstract String getRequestUrl();

    /**
     * Get the value of the Content-Type header field of the HTTP request
     *
     * @return The value of the Content-Type header
     */
    protected abstract String getContentType();

    /**
     * Do the HTTP request with the given request content (body).
     *
     * @param requestBody Body of the request to send
     * @return Reply from the HTTP endpoint
     */
    private String executePost(String requestBody) {

        HttpURLConnection connection = null;

        try {

            String requestURL = getRequestUrl();
            log("Sending request. Target URL: " + requestURL + ". Body: " + requestBody);

            URL url = new URL(getRequestUrl());
            connection = (HttpURLConnection) url.openConnection();

            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", getContentType());
            connection.setRequestProperty("Content-Length", Integer.toString(requestBody.getBytes().length));

            connection.setUseCaches(false);
            connection.setDoOutput(true);

            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(requestBody);
            wr.close();

            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            StringBuilder response = new StringBuilder();
            String line;
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();
            log("Request sent, response: " + response.toString());

            return response.toString();

        } catch (Exception e) {
            log("Request failed, reason: " + e.getMessage());
            return null;
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    /**
     * Do an async HTTP request with the given request content (body).
     *
     * @param requestBody Body of the request to send
     */
    private void executePostAsynchronously(String requestBody) {
        Thread postExecutorThread = new PostExecutorThread(requestBody);
        postExecutorThread.start();
    }

    /**
     * Thread to do asynchronous HTTP requests.
     * Uses the {executePost} method to do the request.
     */
    private class PostExecutorThread extends Thread {

        /**
         * Body of the request to send.
         */
        private final String requestBody;

        /**
         * Create the thread that calls the {executePost} method.
         *
         * @param requestBody Body of the request to send
         */
        public PostExecutorThread(String requestBody) {
            this.requestBody = requestBody;
        }

        /**
         * Do the request.
         */
        @Override
        public void run() {
            executePost(requestBody);
        }

    }

    /**
     * Tag used after the {HTTP_LOG_TAG} tag when logging.
     */
    protected String customLogTag = "";

    /**
     * Tag used for every log message before the custom log tag.
     */
    private final static String HTTP_LOG_TAG = "[FORWARDER:HTTP]";

    /**
     * Write a tagged log message to the console.
     * Newline characters in the message are converted by {StringUtils.replaceNewlines}.
     *
     * @param message Message to log
     */
    protected void log(@NotNull String message) {
        message = StringUtils.replaceNewlines(message);
        System.out.println(HTTP_LOG_TAG + customLogTag + " " + message);
    }

    /**
     * Set the custom log tag.
     *
     * @param customLogTag Tag used after the {HTTP_LOG_TAG} tag when logging.
     *                     If not null, will be converted to [customLogTag] format.
     *                     If null, custom log tag will not be displayed.
     */
    public void setCustomLogTag(@Nullable String customLogTag) {
        if (customLogTag == null)
            this.customLogTag = "";
        else
            this.customLogTag = "[" + customLogTag + "]";
    }

}

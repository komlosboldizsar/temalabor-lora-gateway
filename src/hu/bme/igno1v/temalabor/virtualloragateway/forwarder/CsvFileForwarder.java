package hu.bme.igno1v.temalabor.virtualloragateway.forwarder;

import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;
import hu.bme.igno1v.temalabor.virtualloragateway.StringUtils;
import hu.bme.igno1v.temalabor.virtualloragateway.payload.Variable;
import hu.bme.igno1v.temalabor.virtualloragateway.radio.Frame;

import java.util.ArrayList;
import java.util.List;

/**
 * Class for saving LoRa packets to a CSV file.
 * Every frame is written to a new line.
 * It can be specified at construction if timestamp and device ID should also appear as columns in the CSV file.
 */
public class CsvFileForwarder extends FileForwarder {

    /**
     * Path of the file to write.
     */
    private final String filePath;

    /**
     * Variables that's values will be printed in the file.
     * Every variable is displayed as a column in the CSV file.
     */
    private final List<CsvVariable> variables;

    /**
     * Delimiter used to separate values in a row.
     * Colon (,) and semicolon (;) are common.
     */
    private final char delimiter;

    /**
     * True if timestamp of the frame should be printed in the first column.
     * If not, column doesn't appear as empty column.
     */
    private final boolean printTimestamp;

    /**
     * True if the ID of the device that sent the frame should be printed in the second column.
     * If not, column doesn't appear as empty column.
     */
    private final boolean printDeviceId;

    // Common delimiters

    /**
     * Colon is the most common delimiter for separating values in a row in a CSV file.
     */
    public final static char DELIMITER_COLON = ',';

    /**
     * Semicolon is also used as delimiter in CSV files.
     */
    public final static char DELIMITER_SEMICOLON = ';';

    /**
     * Create the CSV writer object and initialize it's parameters.
     * Sets the custom log tag to {CSV_FILE_LOG_TAG}::filePath.
     *
     * @param filePath       Path of the CSV file to write
     * @param variables      Variables that's values will be printed in the file.
     *                       Every variable is displayed as a column in the CSV file.
     * @param delimiter      Delimiter used to separate values in a row.
     * @param printTimestamp True if timestamp of the frame should be printed in the first column.
     *                       If not, column doesn't appear as empty column.
     * @param printDeviceId  True if the ID of the device that sent the frame should be printed in the second column.
     *                       If not, column doesn't appear as empty column.
     */
    public CsvFileForwarder(@NotNull final String filePath, @Nullable final List<CsvVariable> variables, char delimiter, boolean printTimestamp, boolean printDeviceId) {

        this.filePath = filePath;
        this.delimiter = delimiter;
        this.printTimestamp = printTimestamp;
        this.printDeviceId = printDeviceId;

        if (variables != null)
            this.variables = variables;
        else
            this.variables = new ArrayList<>();

        setCustomLogTag(filePath);

    }

    /**
     * Get the content to write to the CSV file for a frame.
     * Returns exactly one row ending with \n.
     * Every variable from the {variables} member is printed as a column with the given decimals settings
     * using the same order as they are stored in the {variables} field.
     * At the beginning of the line, UNIX timestamp of the frame and sender device ID are also printed
     * if {printTimestamp} and {printDeviceId} are true.
     *
     * @param frame Frame that's contents and meta-informations will be used to create the content string to write
     * @return String to write to the file
     */
    @Override
    protected String getContents(Frame frame) {

        byte[] frmpayload = frame.getFrmpayload();

        List<String> lineParts = new ArrayList<>();

        if (printTimestamp) {
            long timestamp = frame.getReceivedTimestamp().getTime();
            lineParts.add(Long.toString(timestamp));
        }

        if (printDeviceId) {
            byte[] devAddr = frame.getDevaddr();
            String devAddrFormatted = StringUtils.bytesToHex(devAddr, null);
            lineParts.add(devAddrFormatted);
        }

        for (CsvVariable csvVariable : variables) {
            float value = csvVariable.variable.getValue(frmpayload);
            String valueFormat = String.format("%%.%df", csvVariable.decimals);
            String formattedValue = String.format(valueFormat, value);
            lineParts.add(formattedValue);
        }

        String line = String.join(Character.toString(delimiter), lineParts);
        line += '\n';
        return line;

    }

    /**
     * Get the path of the file used for the next write operation.
     *
     * @return Path of the file to write
     */
    @Override
    protected String getFilePath() {
        return filePath;
    }

    /**
     * Get if contents should be appended to the file during the next write operation if it already exists
     *
     * @return true
     */
    @Override
    protected boolean useAppend() {
        return true;
    }

    /**
     * Class for storing data for variables that are written to the CSV file by this forwarder.
     */
    private static class CsvVariable {

        /**
         * Variable that is printed to the file.
         */
        public final Variable variable;

        /**
         * Count of decimals used for printing the value of the variable.
         */
        public final int decimals;

        /**
         * Initialize the structure.
         *
         * @param variable Variable that is printed to the file
         * @param decimals Count of decimals used for printing the value of the variable.
         *                 Must be a non-negative integer.
         * @throws IllegalArgumentException If value of decimals parameter is invalid (<0).
         */
        public CsvVariable(@NotNull final Variable variable, int decimals) throws IllegalArgumentException {

            if (decimals < 0)
                throw new IllegalArgumentException("Value of the decimals parameter must be a non-negative integer!");

            this.variable = variable;
            this.decimals = decimals;

        }

    }

    /**
     * Builder class for constucting easily CsvFileForwarder objects.
     */
    public static class Builder {

        /**
         * Default value for the {delimiter} field.
         */
        private final static char DEFAULT_DELIMITER = DELIMITER_COLON;

        /**
         * Default value for the {printTimestamp} field.
         */
        private final static boolean DEFAULT_PRINT_TIMESTAMP = false;

        /**
         * Default value for the {printDeviceId} field.
         */
        private final static boolean DEFAULT_PRINT_DEVICE_ID = false;

        /**
         * Path of the file to write.
         */
        private final String filePath;

        /**
         * Variables that's values will be printed in the file.
         * Every variable is displayed as a column in the CSV file.
         */
        private List<CsvVariable> variables = new ArrayList<>();

        /**
         * Delimiter used to separate values in a row.
         * Colon (,) and semicolon (;) are common.
         */
        private char delimiter = DEFAULT_DELIMITER;

        /**
         * True if timestamp of the frame should be printed in the first column.
         * If not, column doesn't appear as empty column.
         */
        private boolean printTimestamp = DEFAULT_PRINT_TIMESTAMP;

        /**
         * True if the ID of the device that sent the frame should be printed in the second column.
         * If not, column doesn't appear as empty column.
         */
        private boolean printDeviceId = DEFAULT_PRINT_DEVICE_ID;

        /**
         * Initialize the builder
         *
         * @param filePath Path of the file to write
         */
        public Builder(String filePath) {
            this.filePath = filePath;
        }

        /**
         * Build the CsvFileForwarder object and get.
         *
         * @return The CsvFileForwarder built
         */
        public CsvFileForwarder build() {
            return new CsvFileForwarder(filePath, variables, delimiter, printTimestamp, printDeviceId);
        }

        /**
         * Add a variable that should also be written to the CSV file.
         *
         * @param variable Variable to print to the CSV file as a column
         * @param decimals Count of decimals used for formatting the value
         * @return This builder object for easily setting more things in a row.
         * @throws IllegalArgumentException If value of decimals parameter is invalid (<0).
         */
        public Builder addVariable(@NotNull final Variable variable, int decimals) throws IllegalArgumentException {
            variables.add(new CsvVariable(variable, decimals));
            return this;
        }

        /**
         * Set the delimiter used in CSV files.
         *
         * @param delimiter Delimiter used to separate columns in a row
         * @return This builder object for easily setting more things in a row.
         */
        public Builder setDelimiter(char delimiter) {
            this.delimiter = delimiter;
            return this;
        }

        /**
         * Set the value of the {printTimestamp} field.
         *
         * @param printTimestamp true if timestamp of the frame should also be printed in the CSV file as a column
         * @return This builder object for easily setting more things in a row.
         */
        public Builder setPrintTimestamp(boolean printTimestamp) {
            this.printTimestamp = printTimestamp;
            return this;
        }

        /**
         * Set the value of the {printDeviceId} field.
         *
         * @param printDeviceId true if the ID of the sending device should also be printed in the CSV file as a column
         * @return This builder object for easily setting more things in a row.
         */
        public Builder setPrintDeviceId(boolean printDeviceId) {
            this.printDeviceId = printDeviceId;
            return this;
        }

    }

    /**
     * Tag used for every log message before the custom log tag.
     */
    private final static String CSV_FILE_LOG_TAG = "CSV";

    /**
     * Set the custom log tag.
     *
     * @param customLogTag Tag used with the {CSV_FILE_LOG_TAG} tag when logging.
     *                     If not null, {CSV_FILE_LOG_TAG} and custom log tag will be separated by ::.
     *                     If null, only the {CSV_FILE_LOG_TAG} is shown.
     */
    public void setCustomLogTag(@Nullable String customLogTag) {
        if (customLogTag == null)
            super.setCustomLogTag(CSV_FILE_LOG_TAG);
        else
            super.setCustomLogTag(CSV_FILE_LOG_TAG + "::" + customLogTag);
    }

}

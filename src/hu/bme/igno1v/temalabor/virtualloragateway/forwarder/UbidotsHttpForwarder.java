package hu.bme.igno1v.temalabor.virtualloragateway.forwarder;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;
import hu.bme.igno1v.temalabor.virtualloragateway.payload.Variable;
import hu.bme.igno1v.temalabor.virtualloragateway.radio.Frame;

import java.util.LinkedList;
import java.util.List;

public class UbidotsHttpForwarder extends HttpForwarder {

    /**
     * URL of the Ubidots HTTP API for uploading values for more variables at once.
     */
    private static final String UBIDOTS_HTTPAPI_URL = "http://things.ubidots.com/api/v1.6/collections/values";

    /**
     * Type of the content sent to the HTTP endpoint.
     */
    private static final String CONTENT_TYPE = "application/json";

    /**
     * Token for authorizing the user during API requests.
     */
    private final String token;

    /**
     * Variables sent to the Ubidots platform.
     * All variables are sent in one request.
     */
    private final List<UbidotsVariable> variables = new LinkedList<>();

    /**
     * Initialize the HTTP forwarder and fill needed constant fields.
     * Sets the custom log tag to the value of UBITODS_HTTP_LOG_TAG.
     *
     * @param token Token for authorizing the user during API requests
     */
    public UbidotsHttpForwarder(String token) {
        this.token = token;
        setCustomLogTag(null);
    }

    /**
     * Add a variable that's value should sent to the Ubidots platform when a new LoRa frame arrives.
     *
     * @param variable The variable object where value is read from
     * @param id       The generated id used to identify the variable
     */
    public void addVariable(@NotNull final Variable variable, @NotNull final String id) {
        variables.add(new UbidotsVariable(variable, id));
    }

    /**
     * Create the JSON string that contains the values and timestamp for all variables in the {variables} field and
     * is sent to the Ubidots HTTP API endpoint through the HTTP request.
     *
     * @param frame Frame to forward
     * @return The JSON object built from the frame as string
     */
    @Override
    protected String createRequestBody(Frame frame) {

        byte[] frmpayload = frame.getFrmpayload();
        long receiveTimestamp = frame.getReceivedTimestamp().getTime();

        JsonArray requestBody = new JsonArray();
        for (UbidotsVariable uvar : variables) {
            JsonObject j = new JsonObject();
            j.addProperty("variable", uvar.id);
            float value = uvar.variable.getValue(frmpayload);
            j.addProperty("value", value);
            j.addProperty("timestamp", receiveTimestamp);
            requestBody.add(j);
        }

        return requestBody.toString();

    }

    /**
     * Get the URL of the HTTP request.
     *
     * @return URL of the HTTP request for sending values of more variables to the Ubidots platform at once
     */
    @Override
    protected String getRequestUrl() {
        return UBIDOTS_HTTPAPI_URL + "/?token=" + token;
    }

    /**
     * Get the value of the Content-Type header field of the HTTP request
     *
     * @return Value of the {CONTENT_TYPE} constant
     */
    @Override
    protected String getContentType() {
        return CONTENT_TYPE;
    }

    /**
     * Class for storing data for variables that are published to the Ubidots platform by this forwarder.
     */
    private class UbidotsVariable {

        /**
         * Variable that's value is sent to the platform.
         */
        public final Variable variable;

        /**
         * The generated id used to identify the variable.
         */
        public final String id;

        /**
         * Initialize the data structure and fill the fields.
         *
         * @param variable Variable that's value is sent to the platform
         * @param id       The generated id used to identify the variable
         */
        public UbidotsVariable(@NotNull final Variable variable, @NotNull final String id) {
            this.id = id;
            this.variable = variable;
        }

    }

    /**
     * Tag used for every log message before the custom log tag.
     */
    private final static String UBITODS_HTTP_LOG_TAG = "UBIDOTS";

    /**
     * Set the custom log tag.
     *
     * @param customLogTag Tag used with the {UBITODS_HTTP_LOG_TAG} tag when logging.
     *                     If not null, {UBITODS_HTTP_LOG_TAG} and custom log tag will be separated by ::.
     *                     If null, only the {UBITODS_HTTP_LOG_TAG} is shown.
     */
    public void setCustomLogTag(@Nullable String customLogTag) {
        if (customLogTag == null)
            super.setCustomLogTag(UBITODS_HTTP_LOG_TAG);
        else
            super.setCustomLogTag(UBITODS_HTTP_LOG_TAG + "::" + customLogTag);
    }

}

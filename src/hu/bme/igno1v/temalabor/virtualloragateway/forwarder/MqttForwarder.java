package hu.bme.igno1v.temalabor.virtualloragateway.forwarder;

import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;
import hu.bme.igno1v.temalabor.virtualloragateway.StringUtils;
import hu.bme.igno1v.temalabor.virtualloragateway.radio.Frame;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttMessage;

/**
 * Abstract base class for forwarding LoRa packets through MQTT.
 */
public abstract class MqttForwarder implements FrameForwarder {

    /**
     * MQTT client used for communication.
     */
    MqttClient mqttClient;

    /**
     * QoS value of MQTT packages.
     */
    private int qos = 0;

    /**
     * Create the MQTT forwarder, initialize the MQTT client with given data,
     * and connect asynchronously to the broker with them.
     *
     * @param brokerURL URL of the MQTT broker
     * @param clientID  ID of the MQTT client
     * @param username  Username used for the MQTT connection
     * @param password  Password used for the MQTT connection
     */
    public MqttForwarder(@NotNull final String brokerURL, @NotNull final String clientID, @NotNull final String username, @NotNull final String password) {
        Thread conncetorThread = new ConnectorThread(brokerURL, clientID, username, password);
        conncetorThread.start();
    }

    /**
     * Class to connect to the MQTT broker asynchronously.
     */
    private class ConnectorThread extends Thread {

        /**
         * URL of the MQTT broker.
         */
        private final String brokerURL;

        /**
         * ID of the MQTT client.
         */
        private final String clientID;

        /**
         * Username used for the MQTT connection.
         */
        private final String username;

        /**
         * Password used for the MQTT connection.
         */
        private final String password;

        /**
         * Save connection parameters.
         *
         * @param brokerURL URL of the MQTT broker
         * @param clientID  ID of the MQTT client
         * @param username  Username used for the MQTT connection
         * @param password  Password used for the MQTT connection
         */
        private ConnectorThread(@NotNull final String brokerURL, @NotNull final String clientID, @NotNull final String username, @NotNull final String password) {
            this.brokerURL = brokerURL;
            this.clientID = clientID;
            this.username = username;
            this.password = password;
        }

        /**
         * Create tht MQTT client and connect to the broker.
         */
        @Override
        public void run() {
            try {
                mqttClient = new MqttClient(brokerURL, clientID);
                MqttConnectOptions connOpts = new MqttConnectOptions();
                connOpts.setCleanSession(true);
                connOpts.setUserName(username);
                connOpts.setPassword(password.toCharArray());
                log(String.format("Connecting to broker %s with username \"%s\" and client ID \"%s\"...", brokerURL, clientID, username));
                mqttClient.connect(connOpts);
                log("Connected");
            } catch (Exception e) {
                log("Connection failed, reason: " + e.getMessage());
            }
        }

    }

    /**
     * Forward the frame through MQTT protocol.
     *
     * @param frame Frame to forward
     */
    public abstract void forwardFrame(Frame frame);

    /**
     * Publish the message given in parameters with given topic through the MQTT client.
     *
     * @param topic   Topic to use for publish
     * @param message Message to publish
     */
    protected void publish(@NotNull final String topic, @NotNull final String message) {

        if ((mqttClient != null) && (!mqttClient.isConnected())) {
            log("Can't publish, not connected");
            return;
        }

        try {
            MqttMessage messageObject = new MqttMessage(message.getBytes());
            messageObject.setQos(qos);
            mqttClient.publish(topic, messageObject);
            log(String.format("Published \"%s\" to %s", message, topic));
        } catch (Exception e) {
            log("Failed to publish, reason: " + e.getMessage());
        }

    }

    /**
     * Publish the message given in parameters with given topic through the MQTT client asynchronously.
     *
     * @param topic   Topic to use for publish
     * @param message Message to publish
     */
    protected void publishAsynchronously(@NotNull final String topic, @NotNull final String message) {
        Thread publisherThread = new PublisherThread(topic, message);
        publisherThread.start();
    }

    /**
     * Thread to do asynchronous MQTT publications.
     * Uses the {publish} method to do the publication.
     */
    private class PublisherThread extends Thread {

        /**
         * Topic to use for publish.
         */
        private final String topic;

        /**
         * Message to publish.
         */
        private final String message;

        /**
         * Create the thread that calls the {publish} method.
         *
         * @param topic   Topic to use for publish
         * @param message Message to publish
         */
        public PublisherThread(@NotNull final String topic, @NotNull final String message) {
            this.topic = topic;
            this.message = message;
        }

        /**
         * Do the publication.
         */
        @Override
        public void run() {
            publish(topic, message);
        }

    }

    /**
     * Tag used after the {MQTT_LOG_TAG}  tag when logging.
     */
    protected String customLogTag = "";

    /**
     * Tag used for every log message before the custom log tag.
     */
    private final static String MQTT_LOG_TAG = "[FORWARDER:MQTT]";

    /**
     * Write a tagged log message to the console.
     * Newline characters in the message are converted by {StringUtils.replaceNewlines}.
     *
     * @param message Message to log
     */
    protected void log(@NotNull String message) {
        message = StringUtils.replaceNewlines(message);
        System.out.println(MQTT_LOG_TAG + customLogTag + " " + message);
    }

    /**
     * Set the custom log tag.
     *
     * @param customLogTag Tag used after the {MQTT_LOG_TAG} tag when logging.
     *                     If not null, will be converted to [customLogTag] format.
     *                     If null, custom log tag will not be displayed.
     */
    public void setCustomLogTag(@Nullable String customLogTag) {
        if (customLogTag == null)
            this.customLogTag = "";
        else
            this.customLogTag = "[" + customLogTag + "]";
    }

}

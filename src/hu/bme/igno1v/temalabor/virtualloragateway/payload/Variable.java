package hu.bme.igno1v.temalabor.virtualloragateway.payload;

/**
 * A single variable with a value that can be sent/published to a LoRa/IoT platform.
 */
public interface Variable {

    /**
     * Calculate and get the value of the variable.
     *
     * @param frmpayload FRMPayload that contains the bytes needed to calculate the value of the variable
     * @return The value extracted and calculated from the given FRMPaylaod.
     */
    float getValue(byte[] frmpayload);

}

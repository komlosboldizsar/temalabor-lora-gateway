package hu.bme.igno1v.temalabor.virtualloragateway.payload;

/**
 * Simple variable that is calculated from a single sequence of bytes of the FRMPayload.
 */
public class SimpleVariable implements Variable {

    /**
     * Start offset of the byte sequence that is the source of the value of the variable.
     */
    public final int offset;

    /**
     * Length of byte sequence that is processed to calculate the value of the variable.
     */
    public final int length;

    /**
     * Count of bytes required to represent the whole part of the transferred value.
     */
    public final int whole;

    /**
     * true if the transferred value is signed and can be negative.
     * If true, the first byte will be processed as the sign: 0xFF if negative, 0x00 otherwise
     */
    public final boolean signed;

    /**
     * Initialize the data structure and fill the members.
     *
     * @param offset Start offset of the byte sequence that is the source of the value of the variable.
     * @param length Length of byte sequence that is processed to calculate the value of the variable.
     * @param whole  Count of bytes required to represent the whole part of the transferred value.
     * @param signed f true, the first byte will be processed as the sign: 0xFF if negative, 0x00 otherwise
     * @throws IllegalArgumentException If offset, length or whole is invalid (<0)
     */
    public SimpleVariable(int offset, int length, int whole, boolean signed) throws IllegalArgumentException {

        if (offset < 0)
            throw new IllegalArgumentException("Offset must be a non-negative integer");
        if (length < 0)
            throw new IllegalArgumentException("Length must be a non-negative integer");
        if (whole < 0)
            throw new IllegalArgumentException("Whole must be a non-negative integer");

        this.offset = offset;
        this.length = length;
        this.whole = whole;
        this.signed = signed;

    }

    /**
     * Extract the [offset,offset+length[ bytes from the FRMPayload and calculate the transferred value ("meaning"):
     * - If {signed} is true, the first byte will be processed as the sign: 0xFF if negative, 0x00 otherwise
     * - The next {whole} bytes will be processed as bytes representing the whole part of the transferred value
     * [0] is the MSB, [WHOLE-1] is the LSB
     * - The remaining bytes are processed as bytes representing the fractional part of the transferred number
     *
     * @param frmpayload FRMPayload that contains the bytes needed to calculate the value of the variable
     * @return The value calculatd from {frmpayload} with the described method.
     */
    public float getValue(byte[] frmpayload) {
        float value = 0;
        int startByte = (signed ? 1 : 0);
        int t = offset + startByte;
        for (int b = startByte, p = 0; b < length; b++, p++) {
            byte rawByte = frmpayload[t++];
            int byteValue = (int) rawByte;
            if (rawByte < 0)
                byteValue += 256;
            value += byteValue * Math.pow(256, whole - p - 1);
        }
        if (signed && (frmpayload[offset] == -1)) /* 0xFF = -1 */
            value *= -1;
        return value;
    }

}

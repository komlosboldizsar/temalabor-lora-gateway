package hu.bme.igno1v.temalabor.virtualloragateway.radio;

import com.sun.istack.internal.NotNull;

import java.util.Date;

/**
 * Class/structure for representing a LoRa frame with all of it's PHYPayload fields.
 */
public class Frame {

    /**
     * Timestamp: when did a RadioChannel receive the LoRa frame?
     */
    private final Date receivedTimestamp;

    /**
     * Bytes representing the PHYPayload of the frame.
     */
    private byte[] phypayload;

    /**
     * Bytes representing the MACPayload field in the PHYPayload field.
     */
    private byte[] macpayload;

    /**
     * Bytes representing the FRMPayload field in the MACPayload field.
     */
    private byte[] frmpayload;

    /**
     * Bytes representing the MHDR field in the PHYPayload field.
     */
    private byte[] mhdr;

    /**
     * Bytes representing the MIC field in the PHYPayload field.
     */
    private byte[] mic;

    /**
     * Bytes representing the FHDR field in the MACPayload field.
     */
    private byte[] fhdr;

    /**
     * Bytes representing the FPort field in the MACPayload field.
     */
    private byte[] fport;

    /**
     * Bytes representing the DevAddr field in the FHDR field.
     */
    private byte[] devaddr;

    /**
     * Bytes representing the FCtrl field in the FHDR field.
     */
    private byte[] fctrl;

    /**
     * Bytes representing the FCnt field in the FHDR field.
     */
    private byte[] fcnt;

    /**
     * Bytes representing the FOpts field in the FHDR field.
     */
    private byte[] fopts;

    /**
     * Byte storing the value of the MType field-part of the MHDR field.
     * Only the 3 lower bits are used.
     */
    private byte mtype;

    /**
     * Byte storing the value of the Major field-part of the MHDR field.
     * Only the 3 lower bits are used.
     */
    private byte major;

    /**
     * Member storing the value of the ADR field-part of the FCtrl field.
     */
    private boolean adr;

    /**
     * Member storing the value of the ADRACKReq field-part of the FCtrl field.
     */
    private boolean adrackreq;

    /**
     * Member storing the value of the ACK field-part of the FCtrl field.
     */
    private boolean ack;

    /**
     * Byte storing the value of the FOptsLen field-part of the FCtrl field.
     * Only the 4 lower bits are used.
     */
    private byte foptslen;

    /**
     * Length of the MHDR field in bytes by the LoRaWAN Specification 1.0.
     */
    private static final int LENGTH_MHDR = 1;

    /**
     * Length of the MIC field in bytes by the LoRaWAN Specification 1.0.
     */
    private static final int LENGTH_MIC = 4;

    /**
     * Length of the DevAddr field in bytes by the LoRaWAN Specification 1.0.
     */
    private static final int LENGTH_DEVADDR = 4;

    /**
     * Length of the FCtrl field in bytes by the LoRaWAN Specification 1.0.
     */
    private static final int LENGTH_FCTRL = 1;

    /**
     * Length of the FCnt field in bytes by the LoRaWAN Specification 1.0.
     */
    private static final int LENGTH_FCNT = 2;

    /**
     * Length of the FPort field in bytes by the LoRaWAN Specification 1.0.
     */
    private static final int LENGTH_FPORT = 1;

    /**
     * Create the frame from the given PHYPayload and extract all fields under PHYPayload.
     *
     * @param phypayload        PHYPayload data of the frame
     * @param receivedTimestamp Timestamp of the receiving event of the given PHYPayload
     */
    public Frame(@NotNull byte[] phypayload, @NotNull final Date receivedTimestamp) {

        this.phypayload = phypayload;
        this.receivedTimestamp = receivedTimestamp;

        // Extract fields from PHYPayload
        this.mhdr = subArrayByLength(phypayload, 0, LENGTH_MHDR);
        this.macpayload = subArrayByLength(phypayload, 1, phypayload.length - LENGTH_MHDR - LENGTH_MIC);
        this.mic = subArrayByLength(phypayload, phypayload.length - LENGTH_MIC, 4);

        // Extract fields from MACPayload
        int foptslen = (macpayload[LENGTH_DEVADDR] & 0x0F); // Get FOptsLen from FCtrl
        int fhdrlen = LENGTH_DEVADDR + LENGTH_FCTRL + LENGTH_FCNT + foptslen;
        this.fhdr = subArrayByLength(macpayload, 0, fhdrlen);
        this.fport = subArrayByLength(macpayload, fhdrlen, LENGTH_FPORT);
        this.frmpayload = subArrayByLength(macpayload, fhdrlen + LENGTH_FPORT, macpayload.length - fhdrlen - LENGTH_FPORT);

        // Extract fields from MHDR
        this.mtype = (byte) (mhdr[0] >> 5);
        this.major = (byte) (mhdr[0] & 0x03);

        // Extract fields from FHDR
        int t = 0;
        this.devaddr = subArrayByLength(fhdr, t, LENGTH_DEVADDR);
        t += LENGTH_DEVADDR;
        this.fctrl = subArrayByLength(fhdr, t, LENGTH_FCTRL);
        t += LENGTH_FCTRL;
        this.fcnt = subArrayByLength(fhdr, t, LENGTH_FCNT);
        t += LENGTH_FCNT;
        this.fopts = subArrayByLength(fhdr, t, fhdr.length - t);

        // Extract fields from FCTRL
        this.adr = (((fctrl[0] >> 7) & 0x01) == 1);
        this.adrackreq = (((fctrl[0] >> 6) & 0x01) == 1);
        this.ack = (((fctrl[0] >> 5) & 0x01) == 1);
        this.foptslen = (byte) (fctrl[0] & 0x0F);

    }

    /**
     * Helper method for extracting a sub-sequence from a byte array with given offset (start) and length.
     *
     * @param original Byte array to extract sub-sequence from
     * @param start    Start offset of the sub-sequence to extract
     * @param length   Length of the byte sub-sequence to extract
     * @return The extracted sub-array: bytes [start,start+length[ from {original}
     */
    @NotNull
    private static byte[] subArrayByLength(@NotNull byte[] original, int start, int length) {
        byte[] result = new byte[length];
        for (int i = 0; i < length; i++)
            result[i] = original[start + i];
        return result;
    }

    /**
     * Get the timestamp of the frame.
     *
     * @return When was the frame received
     */
    public Date getReceivedTimestamp() {
        return receivedTimestamp;
    }

    /**
     * Get the element of the MType enumeration that represents the value of the MType field of the frame
     * in a more-human-readable form.
     *
     * @return The element from the MType enumeration that represents the value of the {mtype} field.
     */
    public MType getMytpeEnum() {
        return MType.getByCode(mtype);
    }

    /**
     * Get the value of the PHYPayload field.
     *
     * @return Value of the {phypayload} field.
     */
    public byte[] getPhypayload() {
        return phypayload;
    }

    /**
     * Get the value of the PHYPayload->MACPayload field.
     *
     * @return Value of the {macpayload} field.
     */
    public byte[] getMacpayload() {
        return macpayload;
    }

    /**
     * Get the value of the PHYPayload->MACPayload->FRMPayload field.
     *
     * @return Value of the {frmpayload} field.
     */
    public byte[] getFrmpayload() {
        return frmpayload;
    }

    /**
     * Get the value of the PHYPayload->MHDR field.
     *
     * @return Value of the {mhdr} field.
     */
    public byte[] getMhdr() {
        return mhdr;
    }

    /**
     * Get the value of the PHYPayload->MIC field.
     *
     * @return Value of the {mic} field.
     */
    public byte[] getMic() {
        return mic;
    }

    /**
     * Get the value of the PHYPayload->MACPayload->FHDR field.
     *
     * @return Value of the {fhdr} field.
     */
    public byte[] getFhdr() {
        return fhdr;
    }

    /**
     * Get the value of the PHYPayload->MACPayload->FPort field.
     *
     * @return Value of the {fport} field.
     */
    public byte[] getFport() {
        return fport;
    }

    /**
     * Get the value of the PHYPayload->MACPayload->FHDR->DevAddr field.
     *
     * @return Value of the {devaddr} field.
     */
    public byte[] getDevaddr() {
        return devaddr;
    }

    /**
     * Get the value of the PHYPayload->MACPayload->FHDR->FCtrl field.
     *
     * @return Value of the {fctrl} field.
     */
    public byte[] getFctrl() {
        return fctrl;
    }

    /**
     * Get the value of the PHYPayload->MACPayload->FHDR->FCnt field.
     *
     * @return Value of the {fcnt} field.
     */
    public byte[] getFcnt() {
        return fcnt;
    }

    /**
     * Get the value of the PHYPayload->MACPayload->FHDR->FOpts field.
     *
     * @return Value of the {fopts} field.
     */
    public byte[] getFopts() {
        return fopts;
    }

    /**
     * Get the value of the PHYPayload->MHDR->MType sub-field.
     *
     * @return Value of the {mtype} field.
     */
    public byte getMtype() {
        return mtype;
    }

    /**
     * Get the value of the PHYPayload->MHDR->Major sub-field.
     *
     * @return Value of the {major} field.
     */
    public byte getMajor() {
        return major;
    }

    /**
     * Get the value of the PHYPayload->MACPayload->FHDR->FCtrl->ADR sub-field.
     *
     * @return Value of the {adr} field.
     */
    public boolean getAdr() {
        return adr;
    }

    /**
     * Get the value of the PHYPayload->MACPayload->FHDR->FCtrl->ADRACKReq sub-field.
     *
     * @return Value of the {adrackreq} field.
     */
    public boolean getAdrackreq() {
        return adrackreq;
    }

    /**
     * Get the value of the PHYPayload->MACPayload->FHDR->FCtrl->ACK sub-field.
     *
     * @return Value of the {ack} field.
     */
    public boolean getAck() {
        return ack;
    }

    /**
     * Get the value of the PHYPayload->MACPayload->FHDR->FCtrl->FOptsLen sub-field.
     *
     * @return Value of the {foptslen} field.
     */
    public byte getFoptslen() {
        return foptslen;
    }

    /**
     * Enumeration that represents the possible message types in the
     * order of they codes used in the PHYPayload->MHDR->MType sub-field.
     */
    public enum MType {

        JOIN_REQUEST,
        JOIN_ACCEPT,
        UNCONFIRMED_DATA_UP,
        UNCONFIRMED_DATA_DOWN,
        CONFIRMED_DATA_UP,
        CONFIRMED_DATA_DOWN,
        RFU,
        PROPRIETARY,
        UNKNOWN;

        /**
         * Convert the value of the MType field to an element of this enumeration.
         *
         * @param code Value of the MType field. Valid values are in the [0,7] interval.
         * @return Message type that is represented by {code}, or UNKNOWN if code is invalid
         */
        public static MType getByCode(byte code) {
            if ((code >= 0) && (code <= 7))
                return values()[code];
            return UNKNOWN;
        }

    }

}

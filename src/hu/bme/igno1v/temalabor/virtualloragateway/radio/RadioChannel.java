package hu.bme.igno1v.temalabor.virtualloragateway.radio;

import com.sun.istack.internal.NotNull;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.Date;

/**
 * Part of the virtual LoRa radio interface that represents a receiver tuned to a frequency.
 * Receives LoRa frames through UDP. A channel/frequency is represented by a UDP channel.
 * Each UDP packet represents a LoRa frame and must contain the full PHYPayload.
 */
public class RadioChannel {

    /**
     * The radio that contains this channel module.
     */
    private final Radio owner;

    /**
     * The number listened UDP port.
     */
    private final int socketPortNumber;

    /**
     * Stores if listening the UDP port with a socket is enabled.
     * If enabled, a thread is created and it constantly receives the packets.
     * If disabled, thread is terminated.
     */
    private boolean enabled = false;

    /**
     * Thread that listens UDP port when channel is enabled.
     */
    private ListenerThread listenerThread;

    /**
     * Create the radio channel and initialize constant fields.
     *
     * @param owner            The radio that contains this channel module
     * @param socketPortNumber The number of the listened UDP port (must be between 1 and 65535)
     * @throws IllegalArgumentException When socketPortNumber is invalid
     */
    public RadioChannel(@NotNull Radio owner, int socketPortNumber) throws IllegalArgumentException {

        if (socketPortNumber < 1 || socketPortNumber > 65535)
            throw new IllegalArgumentException("Socket port number must be between 1 and 65535");

        this.owner = owner;
        this.socketPortNumber = socketPortNumber;

    }

    /**
     * Enable the radio: create a listener thread and start listening for UDP packet
     * on the {socketPortNumber} with a socket.
     */
    public void enableRadio() {
        if (!this.enabled) {
            this.enabled = true;
            listenerThread = new ListenerThread();
            listenerThread.start();
        }
    }

    /**
     * Disable the radio: terminate the socket listener thread.
     */
    public void disableRadio() {
        if (this.enabled) {
            this.enabled = false;
            listenerThread.interrupt();
        }
    }

    /**
     * Class for listening for packets on the UDP socket with port {socketPortNumber} without blocking the main thread.
     */
    private class ListenerThread extends Thread {

        /**
         * true while this thread must continuously listen the UDP socket with port {socketPortNumber}.
         */
        private boolean running = true;

        /**
         * Create the UDP socket and listen to it continuously.
         * Notify the {owner} Radio when a new LoRa frame is received.
         */
        public void run() {

            DatagramSocket serverSocket;
            try {
                serverSocket = new DatagramSocket(socketPortNumber);
            } catch (Exception e) {
                System.out.println("[RADIOCHANNEL:" + socketPortNumber + "] Socket creation error!");
                return;
            }

            byte[] receiveData = new byte[300];
            while (running) {

                try {

                    DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
                    serverSocket.receive(receivePacket);

                    int phypayloadLength = receivePacket.getLength();
                    byte[] phypayload = new byte[phypayloadLength];
                    for (int f = 0; f < phypayloadLength; f++)
                        phypayload[f] = receiveData[f];

                    owner.frameReceived(RadioChannel.this, phypayload, new Date());

                } catch (IOException e) {
                    System.out.println("[RADIOCHANNEL:" + socketPortNumber + "] IOException when receiving a UDP packet: " + e.getMessage());
                }

                if (Thread.interrupted())
                    running = false;

            }

            serverSocket.close();

        }
    }

}

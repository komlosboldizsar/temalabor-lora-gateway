package hu.bme.igno1v.temalabor.virtualloragateway.radio;

import com.sun.istack.internal.NotNull;

import java.util.*;

/**
 * Virtual LoRa radio interface.
 */
public class Radio {

    /**
     * Channels that are listened by this radio.
     */
    private final Map<Integer, RadioChannel> channels = new TreeMap<>();

    /**
     * Create a new channel for this radio.
     *
     * @param socketPortNumber UDP port representing the channel (frequency) and that will be listened.
     *                         Must be between 1 and 65535.
     * @throws IllegalArgumentException When socketPortNumber is invalid
     */
    public void addChannel(int socketPortNumber) throws IllegalArgumentException {
        if (!channels.containsKey(socketPortNumber)) {
            RadioChannel ch = new RadioChannel(this, socketPortNumber);
            ch.enableRadio();
            channels.put(socketPortNumber, ch);
        }
    }

    /**
     * Method for the RadioChannel class to notify the radio that a new frame is received through the channel.
     *
     * @param channel    Channel that received the LoRa packet
     * @param phypayload Received PHYPayload
     * @param timestamp  Timestamp when frame was received
     */
    void frameReceived(RadioChannel channel, @NotNull byte[] phypayload, Date timestamp) {
        Frame frm = new Frame(phypayload, timestamp);
        noticeListenersAboutFrameReceived(frm);
    }

    /* Listeners/observers */

    /**
     * List of listeners of this radio.
     * When a new frame is received through this radio, they all will be notified.
     */
    private final List<RadioEventListener> listeners = new LinkedList<>();

    /**
     * Register a new listener for the events of radio.
     * When a frame is received, the newly added subscriber will be notified.
     *
     * @param listener The listener implementing the RadioEventLitener interface to subscribe
     */
    public void registerListener(@NotNull RadioEventListener listener) {
        listeners.add(listener);
    }

    /**
     * Unregister a listener of this radio.
     * The unsubsrcibed object will not be notified when the radio receives a frame.
     *
     * @param listener The listener implementing the RadioEventLitener interface to unsubscribe
     */
    public void unregisterListener(@NotNull RadioEventListener listener) {
        listeners.removeAll(Collections.singleton(listener));
    }

    /**
     * Notify all registered listeners that the radio has received a new frame.
     *
     * @param frame The frame that was received
     */
    public void noticeListenersAboutFrameReceived(Frame frame) {
        for (RadioEventListener l : listeners)
            l.frameReceived(frame);
    }

    /**
     * Interface for observing the events of the radio.
     */
    public interface RadioEventListener {
        void frameReceived(Frame frame);
    }

}

package hu.bme.igno1v.temalabor.virtualloragateway;

import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;

/**
 * Utility class for converting strings.
 *
 * @source https://stackoverflow.com/a/9855338
 */
public class StringUtils {

    /**
     * Array of hexadecimal characters.
     */
    private final static char[] hexArray = "0123456789ABCDEF".toCharArray();

    /**
     * Default separator used to separate bytes when representing them as hex string.
     */
    private final static char DEFAULT_HEX_SEPARATOR = ' ';

    /**
     * Convert bytes to hexadecimal representation.
     *
     * @param bytes     Bytes to convert.
     * @param separator Character used to separate the bytes in the string. If null, no separator will be used.
     * @return String with length of #{bytes}*3-1 or #{bytes}*2 (using separator or not) that contains
     * the hexadecimal representation of the bytes in the {bytes} parameter. [0] is the first (from left) in the string.
     */
    @NotNull
    public static String bytesToHex(@NotNull byte[] bytes, @Nullable Character separator) {

        if (bytes.length < 1)
            return "";

        int charsPerByte = (separator != null) ? 3 : 2;
        int charArrayLength = bytes.length * charsPerByte;
        if (separator != null)
            charArrayLength--;

        char[] hexChars = new char[charArrayLength];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * charsPerByte] = hexArray[v >>> 4];
            hexChars[j * charsPerByte + 1] = hexArray[v & 0x0F];
            if ((separator != null) && (j != bytes.length - 1))
                hexChars[j * charsPerByte + 2] = ' ';
        }

        return new String(hexChars);

    }

    /**
     * Convert bytes to hexadecimal representation without separator.
     *
     * @param bytes Bytes to convert.
     * @return String with length of #{bytes}*2 that contains the hexadecimal representation of the bytes in
     * the {bytes} parameter. [0] is the first (from left) in the string.
     */
    @NotNull
    public static String bytesToHex(@NotNull byte[] bytes) {
        return bytesToHex(bytes, DEFAULT_HEX_SEPARATOR);
    }

    /**
     * Convert a byte to hexadecimal representation.
     *
     * @param singleByte Byte to convert.
     * @return String with length of 2that contains the hexadecimal representation of the given byte.
     */
    @NotNull
    public static String bytesToHex(@NotNull byte singleByte) {
        byte[] byteArray = new byte[]{singleByte};
        return bytesToHex(byteArray, null);
    }

    /**
     * Convert newline characters (\n and \r) to "{\n}" and "{\r}".
     *
     * @param original Original string where newline characters must be replaced
     * @return String where newline characters are replaced as described above
     */
    public static String replaceNewlines(@NotNull String original) {
        return original.replace("\n", "{\\n}").replace("\r", "{\\r}");
    }

}

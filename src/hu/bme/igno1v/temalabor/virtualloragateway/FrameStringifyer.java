package hu.bme.igno1v.temalabor.virtualloragateway;

import com.sun.istack.internal.NotNull;
import hu.bme.igno1v.temalabor.virtualloragateway.radio.Frame;

import java.text.SimpleDateFormat;
import java.util.Base64;

/**
 * Utility class for converting a LoRa frame to string for easily analyzing it's fields.
 */
public class FrameStringifyer {

    /**
     * Date formatter used to format the receiving timestamp of the frame.
     */
    private final static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy.MM.dd. HH:mm:ss");

    /**
     * Convert a LoRa frame to string and show values of all of it's field under the PHYPayload field.
     *
     * @param frm Frame to convert.
     * @return The string built from frame data.
     * Fields are represented in hexadecimal form byte-by-byte in hierarchical structure.
     */
    public static String stringify(@NotNull Frame frm) {

        StringBuilder output = new StringBuilder();

        output.append("## LORA FRAME DETAILS ##");
        output.append("\nReceived: " + DATE_FORMAT.format(frm.getReceivedTimestamp()));

        byte[] phypayload = frm.getPhypayload();
        String phypayloadB64 = new String(Base64.getEncoder().encode(phypayload));
        byte[] frmpayload = frm.getFrmpayload();

        output.append("\n\n-- PAYLOAD RAW FORMATS AND DETAILS --");
        output.append("\nPHYPayload: ").append(StringUtils.bytesToHex(phypayload));
        output.append("\n   BASE64: ").append(phypayloadB64);
        output.append("\n   Length: ").append(phypayload.length);
        output.append("\nMACPayload: ").append(StringUtils.bytesToHex(frm.getMacpayload()));
        output.append("\nFRMPayload: ").append(StringUtils.bytesToHex(frm.getFrmpayload()));
        output.append("\n   Length: ").append(frmpayload.length);

        output.append("\n\n-- HEADER RAW FORMATS --");
        output.append("\nMHDR: ").append(StringUtils.bytesToHex(frm.getMhdr()));
        output.append("\nFHDR: ").append(StringUtils.bytesToHex(frm.getFhdr()));

        output.append("\n\n-- MHDR --");
        output.append("\nMType: ").append(StringUtils.bytesToHex(frm.getMtype())).append(" = ").append(frm.getMytpeEnum());
        output.append("\nMajor: ").append(StringUtils.bytesToHex(frm.getMajor()));

        output.append("\n\n-- FHDR --");
        output.append("\nDevAddr: ").append(StringUtils.bytesToHex(frm.getDevaddr()));
        output.append("\nFCtrl (raw): ").append(StringUtils.bytesToHex(frm.getFctrl()));
        output.append("\n   ADR: ").append(frm.getAdr());
        output.append("\n   ADRACKReq: ").append(frm.getAdrackreq());
        output.append("\n   ACK: ").append(frm.getAck());
        output.append("\n   FOptsLen: ").append(frm.getFoptslen());
        byte[] fcnt = frm.getFcnt();
        int fcntValue = (fcnt[0] >= 0) ? fcnt[0] : (fcnt[0] + 256);
        fcntValue += 256 * ((fcnt[1] >= 0) ? fcnt[1] : (fcnt[1] + 256));
        output.append("\nFCnt: ").append(StringUtils.bytesToHex(fcnt)).append(" = ").append(Integer.toString(fcntValue));
        output.append("\nFOpts (raw): ").append(StringUtils.bytesToHex(frm.getFopts()));

        output.append("\n");

        return output.toString();

    }

}

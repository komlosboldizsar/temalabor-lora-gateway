package hu.bme.igno1v.temalabor.virtualloragateway;

import hu.bme.igno1v.temalabor.virtualloragateway.gateway.Gateway;
import hu.bme.igno1v.temalabor.virtualloragateway.loader.GatewayLoader;
import hu.bme.igno1v.temalabor.virtualloragateway.loader.exception.LoaderException;

/**
 * Main class for the Virtual LoRa Gateway.
 */
public class Application {

    private final static String DEFAULT_CONFIG_FILE = "default.xml";

    /**
     * Entry point of the application.
     *
     * @param args Command line arguments
     *             [0] is the configuration XML file path (relative or absolute).
     *             Optional, if empty, DEFAULT_CONFIG_FILE will be loaded.
     */
    public static void main(String[] args) {
        String xmlPath = (args.length >= 1) ? args[0] : DEFAULT_CONFIG_FILE;
        GatewayLoader gatewayLoader = new GatewayLoader(xmlPath);
        try {
            Gateway gateway = gatewayLoader.load();
        } catch (LoaderException e) {
            e.printStackTrace();
        }
    }

}
